<!DOCTYPE html>

<?php
  $expr1 = $_POST["expr1"];
  $expr2 = $_POST["expr2"];

  //CODE FOR COMPARE EXPRESSIONS PAGE
  
  //CODE FOR COMPARE EXPRESSIONS PAGE

//increment the binary representation of the term variables
//@param $a: the array of variables to represent a term
//@return $a: the newly incremented array of variables
function inc($a)//where a is an array
{
    $length=count($a);//get length of array
    
    //assume not given all 1's
    if($a[$length-1]==0)//if LSB is low
    {
        $a[$length-1]=1;//set LSB high
        return $a;//return the incremented array
    }
   //else if LSB not low
   
   $i=$length-1;//set index to next LSB
   while($a[$i]==1)//while next LSB is high
   {
       
       $a[$i]=0;//set next LSB low
       $i--;//go to next LSB
   }
   if($a[$i]==0)//if LSB is low
   {
       $a[$i]=1;//set LSB high
   }
   return $a;//return the incremented array
}

//Standardizes the various possible input notations of a user to php understand boolean syntax
//@param $expr: the expression to standardize
//@retun $expr: the newly standardized expression
function fixformat($expr)
{

    //Fix the XORS
    $xor=array('XOR','Xor ','xor');
    $expr=str_replace($xor,'^',$expr);
    
    //Fix the ORs
    $or=array('+','or','OR','Or');
    $expr=str_replace($or,'||',$expr);
    
    //Fix the ANDS
    $and=array('.','AND','and','And');
    $expr=str_replace($and,'&&',$expr);
    
    //Fix the parenthases
    $expr=str_replace('[','(',$expr);
    $expr=str_replace(']',')',$expr);
    
    //Fix the Spaces
    $expr=str_replace(' ','',$expr);

    return $expr;//return the standardized expression
}

//Function to replace all multiple spaces with single spaces so the varlist function has no empty elements
//Recursion could be used, however this is faster for the server to process
//@param $string: the string that may contain multiple spaces
//@return $string: the newly modified string, with no duplicate spaces
function single($string)
{
    $length=strlen($string);//start off at max possible value
    for($i=$length;$i>1;$i--)//decrement each time
    {
        if(strpos($string,str_repeat(' ', $i))!==false)//if X number of spaces exist                                 concurrently
        {
            //echo $string."\n"; for testing only
            $string=str_replace( (str_repeat(' ',$i)),' ',$string);//replace the X spaces with a single space
        }
    }
    return $string;//return the modified string, with no multiple spaces
}

//Function to creat an array of individual variables inside the expression, one of each element only
//@param $expr: the expression to create a list of variables for
//@return $x: the list of unique variables in the expression
function varlist($expr)
{
    //Any acceptable character excluding the actual variable names goes here
    $delimiter=array('||','&&','^',')','(','[',']','\n','\t','!');
    
    //Replace all non-variables with spaces
    $i=str_replace($delimiter,' ',$expr);
    
    //Replace all double spaces with single spaces
    $i=single($i);//removes all mutiples of spaces for formatting purposes
    
    //Creates array of variables, no empties, may have duplicates
    $stack = explode(' ', $i);//divide string into array, seperate at each space
    
    //remove duplicates
    $stack= array_unique($stack);//make array unique
    foreach($stack as $key=>$value)//remove empty element entries
    {
        if ($value=="")//if element is blank
        {
            unset($stack[$key]);//remove from list
        }
    }
    
    $x=array();//create blank array for answer list
    
    //fix the indices of the list by copying the current one, element-by-element
    foreach($stack as $value)
    {
        array_push($x,$value);//push onto new array
    }
    
    return $x;//returns array of variables 
}




//creates a list of all variables for both expressions, assumes already gotten correct varlist from both
//@param $list1:the list of variables from the first expression
//@param $list2:the list of variables from the second expression
//@return $master: the combined list of variables from both starting lists
function masterlist($list1,$list2)
{
    if (count($list1)>count($list2))//set size of array
    {
        //set the list to base upon, and the list to add to
        $master=$list1;
        $slave=$list2;
    }
    else
    {
        //set the list to base upon, and the list to add to
        $master=$list2;
        $slave=$list1;
    }
    
    foreach($slave as $value)//for each element in the smaller array
    {
        if (!in_array($value, $master)) //if the element is not included in the master
        {
            array_push($master,$value);//add to the masterlist
        }
    }
    return $master;//return the list of all variables for both expressions
}

//collaborates with inc($i) to determine when an array has reached its binary maximum
//@param $i: the array representing the variables binary terms
function ismax($i)//test for 111 at end of loop
{
    foreach($i as $value)
    {
        //can only be max if all digits are set to 1, not 0
        if($value===0)
        {
            return false;//not at max
        }
    }
    return true;//at max
}


//Function evaluates the boolean result of expression where the given list of parameters is true
//@param $expr: the expression to evaluate
//@param $trues: the list of variables set true in the expression evaluation
function result($expr,$trues)
{   
    //replace the variables with their literal boolean values
    $e=str_replace($trues,'true',$expr);//replace the trues
                                        //variables no longer exist in expression
                                        //only false remain
    $e=str_replace(varlist($expr),'false',$e);//all else set false
    
    //Eval
    eval("\$result=$e;");//evaluate string as boolean expression
    return $result;//return boolean result of expression
}


//compare expreson to determine logical equivalence
//@param $expr1: the first expression to compare
//@param $expr2: the second expression to compare
function testall($expr1,$expr2,$list)
{
    $i=array();//
    foreach($list as $value)//create empty array of size varlist
    {
        array_push($i,0);
    }
   while(true)
   {
      //var_dump($i);//testing only
      $trues=array();//clear array
      foreach($i as $key=>$value)//create array of trues based on current i list
      {
          if ($value===1)
          {
              array_push($trues,$list[$key]);
          }
      }
      if (result($expr1,$trues)!=result($expr2,$trues))
      {
            return false;//not same expression
      }
      
      //else
      if(ismax($i))//we got to the last one and none of them are different
      {
          return true;//same expression
      }
      $i=inc($i);
      
   }
           
}  

//the function to compare two boolean expressions
//@param $expr1: the first expression to compare
//@param $expr2: the second expression to compare
//@param $output: the boolean result of the comparison
function compare($expr1,$expr2)
{
    //test to ensure the first expression is valid
    if(count(varlist($expr1))===0)//if the expression contains no variables
    {
        return "The first expression must contain a valid input.";//error
    }
    
    //test to ensure the second expression is valid
    if(count(varlist($expr2))===0)//if the expression contains no variables
    {
        return "The second expression must contain a valid input.";//error
    }

    //fix the format of the expressions
    $expr1=fixformat($expr1);
    $expr2=fixformat($expr2);

    //create the list of variables for both expressions
    $list1=varlist($expr1);
    $list2=varlist($expr2);
    
    //create the master list of variables in both expressions
    $master=masterlist($list1,$list2);
    
    //determine, and return, the result of the comparison
    $output=testall($expr1,$expr2,$master);
    return $output;
}
?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="css/stylesheet.css" rel="stylesheet">
    <title>Compare</title>
  </head>

  <body>
  	<?php include("navbar.html");?>
	
	<div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Compare</h2>
                    <hr class="star-primary">
                </div>
    </div>

  	<div class="container-fluid">
  		<div class="row">
			<div class="col-10 col-sm-10 col-lg-10">
				<form method="post" action="<?php echo $PHP_SELF;?>">
				<textarea class="form-control" name="expr1" rows="10" placeholder="Expression 1"><?php if (isset($_POST['submit'])){echo $expr1;}?></textarea><br />
				<textarea class="form-control" name="expr2" rows="10" placeholder="Expression 2"><?php if (isset($_POST['submit'])){echo $expr2;}?></textarea><br />
				<button type="submit" value="submit" name="submit" class="btn btn-default">Submit</button><br />
				</form>
			</div> <!-- col -->

		<div class="col-2 col-sm-2 col-lg-2">
			  <button type="button" class="btn btn-default" data-toggle="modal" data-target=".help-modal">
				<span class="glyphicon glyphicon-question-sign"></span>
			  </button>
			  <div class="modal fade help-modal" tabindex="-1" role="dialog" aria-labelledby="myHelpModalLabel" aria-hidden="true">
				<div class="modal-dialog">
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					  <h4 class="modal-title" id="myModalLabel">Help</h4>
					</div>
					<div class="modal-body">
					  <p>Enter two different boolean/digital equations in the different boxes, then press GO, then the result of the comparison is shown in the box below. Formatting for expressions allows all standardized notations for logical operators.</p>
					  <p>ANDs can be expressed with AND,And,.,and</p>
					  <p>ORS can be expressed with +, or, OR, Or
					  <p>XORS can be expressed by XOR, Xor, xor,</p>
					</div>
					<div class="modal-footer">
					  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				  </div>
				</div>
			  </div>

			  <div class="row-fluid">
				<h3>Results:</h3>
				<div class="well">
					<?php
								if (isset($_POST["submit"])) 
								{
									if($expr1=="" or $expr2=="") 
									{
									?>
									<img class="img-responsive" src="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcRZhHtQLO7FLczBwMUtCyHjw6obdlE8zenAtPYqPmXk9HI59eil" alt="">
									<?php 
									} else if(compare($expr1,$expr2)){ 
									?>
									<img class="img-responsive" src="/img/same.png">
									<?php 
									} else {
									?>
									<img class="img-responsive" src="/img/different.png">
									<?php
									}
								}
					?>
				</div>
		   </div> <!-- row-fluid -->
        </div> <!-- col -->
	  </div> <!-- row -->
  	</div><!-- container-fluid -->



    <footer align="center">
      <p>Copyright &copy; Josh Cohen-Collier & Brandon To 2015</p>
    </footer>
	
	
  	<script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

  </body>
</html>